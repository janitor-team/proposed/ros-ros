Source: ros-ros
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jochen Sprickerhof <jspricke@debian.org>,
           Leopold Palomo-Avellaneda <leo@alaxarxa.net>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 13),
	       catkin (>= 0.8.10-1~),
	       librospack-dev,
	       libboost-program-options-dev,
	       libboost-filesystem-dev,
	       libboost-thread-dev,
	       libgtest-dev,
	       libtinyxml2-dev,
	       python3-all, python3-setuptools,
	       dh-sequence-python3,
	       libpython3-dev,
	       dh-exec,
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/science-team/ros-ros
Vcs-Git: https://salsa.debian.org/science-team/ros-ros.git
Rules-Requires-Root: no
Homepage: https://wiki.ros.org/ROS

Package: ros-mk
Section: devel
Architecture: all
Depends: ${misc:Depends}, rospack-tools
Multi-Arch: foreign
Description: Robot OS make file helpers
 This package is part of Robot OS (ROS). It is a collection of make
 include files for building ROS architectural elements.  Most package
 authors should use cmake.mk, which calls CMake for the build of the
 package. The other files in this package are intended for use in
 exotic situations that mostly arise when importing 3rdparty code.

Package: rosbuild
Section: devel
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, python3:any
Multi-Arch: foreign
Description: scripts for managing the Robot OS build system
 This package is part of Robot OS (ROS). Rosbuild solves the core
 problem of gathering appropriate build flags from, and tracking
 dependencies in, the ROS package tree. The macros provided by
 rosbuild automatically inherit the union of build flags exported by
 packages on which your package depends.

Package: roslang
Section: devel
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Common metapackage for all Robot OS client libraries
 This package is part of Robot OS (ROS). It is mainly used to find
 client libraries (via 'rospack depends-on1 roslang'). The roslang
 package is only of interest to those implementing a ROS client
 library. Client libraries mark themselves as such by depending on
 the roslang package, which allows rosbuild and other tools to perform
 appropriate actions, such as msg- and srv-based code generation. The
 roslang package itself contains no actual code.

Package: libroslib-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libroslib0d (= ${binary:Version}), ${misc:Depends}, librospack-dev, libboost-thread-dev, ros-environment
Description: development files for roslib (Robot OS)
 This package is part of Robot OS (ROS). It provides the base
 dependencies and support libraries for ROS. roslib contains many of
 the common data structures and tools that are shared across ROS
 client library implementations.
 .
 This package contains the development files for the library.

Package: libroslib0d
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: library for roslib (Robot OS)
 This package is part of Robot OS (ROS). It provides the base
 dependencies and support libraries for ROS. roslib contains many of
 the common data structures and tools that are shared across ROS
 client library implementations.
 .
 This package contains the C library.

Package: python3-roslib
Section: python
Architecture: all
Multi-Arch: allowed
Depends: ${python3:Depends}, ${misc:Depends}, python3-rospkg, python3-catkin
Description: Python 3 library for roslib (Robot OS)
 This package is part of Robot OS (ROS). It provides base dependencies
 and support libraries for ROS. roslib contains many of the common
 data structures and tools that are shared across ROS client library
 implementations.
 .
 This package contains the Python 3 library.

Package: rosbash
Section: utils
Architecture: all
Depends: ${misc:Depends}, rospack-tools, catkin
Recommends: bash-completion
Multi-Arch: foreign
Description: Assorted shell commands for using Robot OS with bash
 This package is part of Robot OS (ROS). The rosbash package contains
 some useful bash functions and adds tab-completion to a large number
 of the basic ros utilities. The package includes limited support for
 zsh and tcsh by way of sourcing the roszsh or rostcsh files
 respectively. It doesn't provide documentation on these shells,
 though much of the functionality is similar to the bash shell
 extensions.

Package: python3-rosboost-cfg
Section: python
Architecture: all
Multi-Arch: allowed
Depends: ${python3:Depends}, ${misc:Depends}
Description: Contains scripts used by the Robot OS rosboost-cfg tool (Python 3)
 This package is part of Robot OS (ROS). It is used for determining
 build flags (cflags/lflags/etc.) for boost on your system when you
 use it in a ROS environment.
 .
 This package contains the Python 3 library.

Package: python3-rosclean
Section: python
Architecture: all
Multi-Arch: allowed
Depends: ${python3:Depends}, ${misc:Depends}, python3-rospkg, python3-distutils
Description: cleanup Robot OS filesystem resources (e.g. logs) (Python 3)
 This package is part of Robot OS (ROS). rosclean purge will remove
 directories associated with storing ROS-related log files. You will
 be asked to confirm each deletion and it is important that you verify
 the command that rosclean purge executes is correct.
 .
 This package contains the Python 3 library.

Package: python3-roscreate
Section: python
Architecture: all
Multi-Arch: allowed
Depends: ${python3:Depends}, ${misc:Depends}, python3-rospkg, python3-setuptools, python3-roslib
Description: Robot OS empty package template creator (Python 3)
 This package is part of Robot OS (ROS). It provides roscreate-pkg
 which creates empty ROS package templates and thus addresses the
 common problem of packages being created using pre-existing packages,
 which leads to errors in build files and manifests. The tool creates a
 new package directory, including the appropriate build and manifest
 files.
 .
 This package contains the Python 3 library.

Package: python3-rosmake
Section: python
Architecture: all
Multi-Arch: allowed
Depends: ${python3:Depends}, ${misc:Depends}, python3-rospkg
Description: rosmake is a Robot OS dependency aware build tool (Python 3)
 This package is part of Robot OS (ROS). rosmake is a tool to assist
 with building ROS packages. It facilitates building packages that
 have dependencies, allowing all dependencies to be built in the
 correct order.
 .
 This package contains the Python 3 library.

Package: python3-rosunit
Section: python
Architecture: all
Multi-Arch: allowed
Depends: ${python3:Depends}, ${misc:Depends}, python3-rospkg
Description: Unit-testing package for ROS (Python 3)
 This package is part of Robot OS (ROS). python3-rosunit is a
 lower-level library for rostest and handles unit tests, whereas
 rostest handles integration tests. It's an internal tool for running
 unit tests within ROS. While it can be run by a regular user, most
 users will generally use rosunit indirectly via rosbuild test macros.
 .
 This package contains the Python 3 library.
